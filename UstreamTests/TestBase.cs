﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UstreamAutomation;

namespace UstreamTests
{
    [TestClass]
    public class TestBase
    {
        [TestInitialize]
        public void Initialize()
        {
            Driver.Initialize();
        }

        [TestCleanup]
        public void Cleanup()
        {
            Driver.Close();
        }
    }
}
