﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UstreamAutomation;
using UstreamAutomation.Dialogs;
using UstreamAutomation.Pages;

namespace UstreamTests
{
    [TestClass]
    public class SignUpTests : TestBase
    {
        [TestInitialize]
        public void Init()
        {
            MainPage.OpenLogInDialog();
            LoginDialog.SignUp();
        }

        [TestMethod]
        public void SignUp_Fill_Invalid_Email()
        {
            SignUpDialog.SignUp("ust_email@mail");

            Assert.IsTrue(SignUpDialog.WrongEmailErrorMessageIsShown);
        }

        [TestMethod]
        public void SignUp_Fill_Valid_Email()
        {
            SignUpDialog.SignUp(Driver.GeneratedUsername);

            Assert.IsFalse(SignUpDialog.WrongEmailErrorMessageIsShown);
        }

        [TestMethod]
        public void SignUp_Terms_of_Service()
        {
            SignUpDialog.OpenTermsOfService();

            Assert.IsTrue(TermsOfService.Opened,
                "Page was not opened.");
        }

        [TestMethod]
        public void SignUp_Privacy_Policy()
        {
            SignUpDialog.OpenPrivacyPolicy();

            Assert.IsTrue(PrivacyPolicy.Opened,
                "Page was not opened.");
        }

        [TestMethod]
        public void SignUp_NoFill_Email()
        {
            SignUpDialog.SignUp("", "testpassword");

            Assert.IsTrue(SignUpDialog.FieldIsRequiredMessageIsShown,
                "Wrong error message or no error message was shown.");
        }

        [TestMethod]
        public void SignUp_NoFill_Password()
        {
            SignUpDialog.SignUp(Driver.GeneratedUsername);

            Assert.IsTrue(SignUpDialog.FieldIsRequiredMessageIsShown,
                "Wrong error message or no error message was shown.");
        }

        [TestMethod]
        public void SignUp_Wrong_Password1()
        {
            SignUpDialog.SignUp(Driver.GeneratedUsername, "1Aa");

            Assert.IsTrue(SignUpDialog.PasswordMustContainMessageIsShown,
                "Wrong error message or no error message was shown.");
        }

        [TestMethod]
        public void SignUp_Wrong_Password2()
        {
            SignUpDialog.SignUp(Driver.GeneratedUsername, "12345");

            Assert.IsTrue(SignUpDialog.PasswordMustContainMessageIsShown,
                "Wrong error message or no error message was shown.");
        }

        [TestMethod]
        public void SignUp_Wrong_Password3()
        {
            SignUpDialog.SignUp(Driver.GeneratedUsername, "1AAAA");

            Assert.IsTrue(SignUpDialog.PasswordMustContainMessageIsShown,
                "Wrong error message or no error message was shown.");
        }

        [TestMethod]
        public void SignUp_Wrong_Password4()
        {
            SignUpDialog.SignUp(Driver.GeneratedUsername, "     ");

            Assert.IsTrue(SignUpDialog.PasswordMustContainMessageIsShown,
                "Wrong error message or no error message was shown.");
        }

        [TestMethod]
        public void SignUp_Wrong_Password5()
        {
            SignUpDialog.SignUp(Driver.GeneratedUsername, "#&@<>");

            Assert.IsTrue(SignUpDialog.PasswordMustContainMessageIsShown,
                "Wrong error message or no error message was shown.");
        }

        [TestMethod]
        public void SignUp_Email_Address_Is_Taken()
        {
            SignUpDialog.SignUp("email@email.com", "123aA");

            Assert.IsTrue(SignUpDialog.EmailAddressIsTakenMessageIsShown,
                "Wrong error message or no error message was shown.");
        }
        
        [TestMethod]
        public void SignUp_Show_Password()
        {
            SignUpDialog.SignUp(Driver.GeneratedUsername, "123aA", false);
            SignUpDialog.ShowPassword();

            Assert.IsTrue(SignUpDialog.PasswordIsShown,
                "Password was not shown.");
        }

        [TestMethod]
        public void SignUp_Successful()
        {
            SignUpDialog.SignUp(Driver.GeneratedUsername, "123aA");

            Assert.IsTrue(EmailConfirmDialog.IsLoaded,
                "Signup was not successful.");
        }
    }
}
