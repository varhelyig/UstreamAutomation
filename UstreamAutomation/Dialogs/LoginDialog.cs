﻿using OpenQA.Selenium;

namespace UstreamAutomation.Dialogs
{
    public class LoginDialog
    {
        public static bool IsAt
        {
            get { return Driver.Instance.FindElement(By.ClassName("classicSignUp")).Displayed; }
        }

        public static void SignUp()
        {
            Driver.Instance.FindElement(By.ClassName("classicSignUp")).Click();
        }
    }
}
