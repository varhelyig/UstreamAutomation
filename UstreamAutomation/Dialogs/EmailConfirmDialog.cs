﻿using OpenQA.Selenium;

namespace UstreamAutomation.Dialogs
{
    public class EmailConfirmDialog
    {
        public static bool IsLoaded
        {
            get
            {
                 return Driver.Instance.FindElements(By.Id("EmailConfirmDialog")).Count > 0;
            }
        }
    }
}
