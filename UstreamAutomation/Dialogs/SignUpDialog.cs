﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace UstreamAutomation.Dialogs
{
    public class SignUpDialog
    {
        public static IWebElement PasswordTextInput = Driver.Instance.FindElement(By.Id("SignUpPassword"));

        private static IWebElement ShowPasswordButton =
            Driver.Instance.FindElement(By.CssSelector("button.toggle-password"));
        private static IWebElement SignUpButton =
            Driver.Instance.FindElement(By.XPath("//div/button[contains(., 'Sign Up')]"));

        private static IWebElement TermsOfServiceLink =
            Driver.Instance.FindElement(By.LinkText("Terms of Service"));
        private static IWebElement PrivacyPolicyLink =
            Driver.Instance.FindElement(By.Id("SignUpDialog")).FindElement(By.LinkText("Privacy Policy"));

        public static IWebElement EmailTextInput = (new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10)))
            .Until(ExpectedConditions.ElementToBeClickable(By.Id("SignUpEmail")));

        public static bool WrongEmailErrorMessageIsShown
        {
            get
            {
                return Driver.Instance.FindElements(By.XPath(
                    "//span[@class='tooltip-error' and contains(., 'Please enter a valid email address')]")).Count > 0;
            }
        }

        public static bool FieldIsRequiredMessageIsShown
        {
            get
            {
                return Driver.Instance.FindElements(By.XPath(
                    "//span[@class='tooltip-error' and contains(., 'This field is required.')]")).Count > 0;
            }
        }

        public static bool PasswordMustContainMessageIsShown
        {
            get
            {
                return Driver.Instance.FindElements(By.XPath(
                    "//span[@class='tooltip-error' and contains(., 'Must contain at least')]")).Count > 0;
            }
        }

        public static bool EmailAddressIsTakenMessageIsShown
        {
            get
            {
                return Driver.Instance.FindElements(By.XPath(
                    "//span[@class='tooltip-error' and contains(., 'Oops this email address is already taken. Please try a different one!')]")).Count > 0;
            }
        }

        public static bool PasswordIsShown
        {
            get
            {
                return Driver.Instance.FindElements(By.XPath("//div[contains(@class, 'show-password')]")).Count > 0;
            }
        }


        public static void SignUp(string email = "", string password = "", bool clickonsignup = true)
        {
            EmailTextInput.SendKeys(email);
            PasswordTextInput.SendKeys(password);
            if (clickonsignup) SignUpButton.Click();
        }

        public static void OpenTermsOfService()
        {
            TermsOfServiceLink.Click();
        }

        public static void OpenPrivacyPolicy()
        {
            PrivacyPolicyLink.Click();
        }

        public static void ShowPassword()
        {
            ShowPasswordButton.Click();
        }
    }
}
