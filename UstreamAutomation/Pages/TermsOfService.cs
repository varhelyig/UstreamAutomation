using OpenQA.Selenium;

namespace UstreamAutomation.Pages
{
    public class TermsOfService
    {
        public static bool Opened
        {
            get
            {
                var browserTabs = Driver.Instance.WindowHandles;
                Driver.Instance.SwitchTo().Window(browserTabs[1]);

                return Driver.Instance.FindElement(By.CssSelector("#terms-and-conditions-for-subscription-software")).Displayed;
            }
            
        }
    }
}