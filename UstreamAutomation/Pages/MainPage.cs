﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using UstreamAutomation.Dialogs;

namespace UstreamAutomation.Pages
{
    public class MainPage
    {
        private const string mainpageURL = "https://www.ustream.tv";

        public static bool IsAt
        {
            get
            {
                return Driver.Instance.FindElements(By.XPath("//div[@id='HeaderLogin']/a")).Count > 0;
            }
        }

        public static void Open()
        {
            Driver.Instance.Navigate().GoToUrl(mainpageURL);
            //var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(0));
            //wait.Until(d => new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10)))
            //.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//div[@id='HeaderLogin']/a")));

        }

        public static void OpenLogInDialog()
        {
            if (!IsAt)
            {
                Open();
            }
            Driver.Instance.FindElement(By.XPath("//div[@id='HeaderLogin']/a")).Click();
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
            wait.Until(d => LoginDialog.IsAt);
        }
    }


}
