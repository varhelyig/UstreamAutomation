﻿

namespace UstreamAutomation.Pages
{
    public class LoginPage
    {
        public static void GoTo()
        {
            Driver.Instance.Navigate().GoToUrl("https://www.ustream.tv/login");
            
        }
    }
}
