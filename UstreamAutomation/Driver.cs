using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace UstreamAutomation
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }

        public static void Initialize()
        {
            KillProcess("chromedriver");
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("start-maximized");
            Instance = new ChromeDriver(options);
            Instance.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));           
        }

        public static void Close()
        {
            Instance.Quit();
            KillProcess("chromedriver");
        }

        public static void KillProcess(string processName)
        {
            IEnumerable<Process> processes = Process.GetProcesses().Where(x => x.ProcessName == processName);

            foreach (Process process in processes)
            {
                try
                {
                    if (!process.HasExited)
                    {
                        process.Kill();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        public static string GeneratedUsername
        {
            get
            {
               return string.Format("ust_{0}@mail.com", DateTime.Now.ToString("yyyyMddHHmmss")); 
            }
        }

    }
}